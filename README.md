# JAX-WS web service demo (contract first)
Java WSDL web service boilerplate code

**Stack**
 
| *Technology*  | *Version* |
| ------------- | ------------- |
| Java | 8 |
| Maven | 3.3 |
| Docker | 1.9 |
| Wildfly | 10.0.0 |

## Running integration tests - continuous delivery style
- `$ mvn -Pcd verify -Dmaven.buildNumber.doCheck=false`

### How to run integration tests - option I
 * Start application (steps: build from source, upload artifact to maven repo, build image, start container)
  - `$ mvn -Pcd pre-integration-test -Dmaven.buildNumber.doCheck=false`
 * Run integration test
  - `$ mvn integration-test`

### How to run integration tests - option II
 - Follow [instructions](https://github.com/tecris/docker/blob/v3.3/nexus/README.md) to add jboss repository (as proxy repository) to nexus

  ```
  $ docker run -d --name jax-demo -p 8080:8080 -p 9990:9990 casadocker/wildfly:10.0.0   # start wildfly container
  $ mvn wildfly:deploy                                                                  # deploy application
  $ mvn wildfly:undeploy                                                                  # undeploy application
  ```

### Test deployment - manually
 - `$ ./postRequest.sh`

### Create tag with maven 
 - `$ mvn -Darguments="-DskipITs" release:prepare`
 - -Darguments="-DskipITs"  - skip integration tests

###  WSDL
- http://localhost:8080/scoreboardService/ScoreboardService?wsdl
