package org.terra.ws.jaxws.contractfirst;

import java.util.logging.Logger;

import javax.jws.WebService;

@WebService(endpointInterface = "org.terra.ws.jaxws.contractfirst.ScoreboardServicePortType", portName = "ScoreboardServicePort", serviceName = "ScoreboardService", targetNamespace = "http://github.com/tecris/org.terra.ws.jaxws.contractfirst/1.0")
public class ScoreboardServicePortImpl implements ScoreboardServicePortType {

    private static final Logger LOGGER = Logger.getLogger(ScoreboardServicePortImpl.class.getName());

    @Override
    public ScoreboardResponse processScoreboard(ScoreboardRequest scoreboardRequest) {
        EventType event = scoreboardRequest.getEvent();
        String out = event.getCompetition() + " -> " + event.getHomeTeamName() + " - " + event.getVisitorTeamName();
        LOGGER.info(out);
        ScoreboardResponse scoreboardResponse = new ScoreboardResponse();
        scoreboardResponse.setEvent(event);
        return scoreboardResponse;
    }

}
